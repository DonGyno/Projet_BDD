<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 09/12/2015
 * Time: 16:18
 */




include('header.php');

if(empty($_SESSION["login_enseignant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("planning.php");
        });
    </script>
    <?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(isset($_GET['action']) && $_GET['action'] == "ajoutSalle")
{
    $lib_salle = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtoupper($_POST["lib_salle"]))));
    if(!empty($lib_salle))
    {
        //Vérification d'une salle ayant le même libellé !
        $selection_salle = mysqli_query($link,"SELECT libelle_salle FROM salle WHERE libelle_salle = '".$lib_salle."' ;") or die(mysqli_error($link));
        $resulats_salle = mysqli_fetch_array($selection_salle);
        if($resulats_salle[0] != $lib_salle)
        {
            mysqli_query($link,"INSERT INTO salle VALUES (NULL,'".$lib_salle."') ;") or die(mysqli_error($link));
            echo "1|La salle a été ajouté avec succès !";
        }
        else
        {
            echo "2|Une salle est déjà attribuée au nom fournie !";
        }
    }
    else
    {
        echo "2|Remplissez le champ !";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "affecterSoutenance")
{
    $date_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["date_sou"])));
    $heure_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["heure_sou"])));
    $id_stage = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_stage"])));
    $id_salle = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_salle"])));
    $id_enseignant = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_enseignant"])));
    $id_coenseignant = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_coenseignant"])));

    if(!empty($date_sou) && !empty($heure_sou) && !empty($id_enseignant) && !empty($id_coenseignant) && !empty($id_salle) && !empty($id_stage))
    {
        if($id_enseignant != $id_coenseignant)
        {
            $query3 = "INSERT INTO soutenance VALUES (NULL,'".$date_sou."','".$heure_sou."','".$id_enseignant."','".$id_coenseignant."','".$id_salle."',NULL);";
            $stmt = $link->prepare($query3);
            $stmt->execute();
            $last_ID_Sou = $stmt->insert_id;

            //Update stage
            mysqli_query($link,"UPDATE stage SET id_sou = '".$last_ID_Sou."' WHERE id_stage = '".$id_stage."' ;") or die(mysqli_error($link));
            echo "1| L'affectation de la soutenance est un succès !";
        }
        else
        {
            echo "2| Erreur ! Impossible d'ajouter 2 fois le même enseignant ! ";
        }
    }
    else
    {
        echo "2| Erreur ! Des champs sont vides ! ";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "choixDate")
{
    $date_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["date_sou"])));
    $heure_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["heure_sou"])));
    if(!empty($date_sou) && !empty($heure_sou))
    {
        $select_salle_soutenance = mysqli_query($link,"SELECT * FROM salle
                                                      WHERE salle.id_salle NOT IN(
                                                      SELECT salle.id_salle FROM salle INNER JOIN soutenance
                                                      WHERE salle.id_salle = soutenance.id_salle
                                                      AND soutenance.heure_sou ='".$heure_sou."' AND soutenance.date_sou = '".$date_sou."')
                                                      ;") or die(mysqli_query($link));
        while($resulat_salle_soutenance = mysqli_fetch_array($select_salle_soutenance))
        {
            echo $resulat_salle_soutenance[0]."-".$resulat_salle_soutenance[1].";";
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] == "choixEnseignant")
{
    $date_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["date_sou"])));
    $heure_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["heure_sou"])));

    if(!empty($date_sou) && !empty($heure_sou))
    {
        $query1 = "SELECT en2.id_enseignant, en2.nom_enseignant, en2.prenom_enseignant  FROM enseignant AS en2
                WHERE en2.id_enseignant NOT IN(
                    SELECT en.id_enseignant FROM enseignant as en
                    INNER JOIN soutenance AS sou
                    WHERE en.id_enseignant = sou.id_enseignant
                    AND sou.date_sou = '".$date_sou."' AND sou.heure_sou = '".$heure_sou."')
        ;";
        $selection_enseignant = mysqli_query($link,$query1) or die(mysqli_error($link));
        while($resultat_selection_enseignant = mysqli_fetch_array($selection_enseignant))
        {
            echo $resultat_selection_enseignant[0]."-".$resultat_selection_enseignant[1]."-".$resultat_selection_enseignant[2].";";
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] == "choixCoEnseignant")
{
    $date_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["date_sou"])));
    $heure_sou = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["heure_sou"])));

    if(!empty($date_sou) && !empty($heure_sou))
    {
        $query2 = "SELECT en2.id_enseignant, en2.nom_enseignant, en2.prenom_enseignant  FROM enseignant AS en2
                    WHERE en2.id_enseignant NOT IN(
                        SELECT en.id_enseignant FROM enseignant AS en
                        INNER JOIN soutenance AS sou
                        WHERE en.id_enseignant = sou.id_enseignant_1
                        AND sou.date_sou = '" . $date_sou . "' AND sou.heure_sou = '" . $heure_sou . "')
                    ;";
        $selection_coenseignant = mysqli_query($link, $query2) or die(mysqli_error($link));
        while ($resultat_selection_coenseignant = mysqli_fetch_array($selection_coenseignant))
        {
            echo $resultat_selection_coenseignant[0]."-".$resultat_selection_coenseignant[1]."-".$resultat_selection_coenseignant[2].";";
        }
    }
}

if(!isset($_GET["action"]))
{

?>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="poster_annonce.php">Poster une annonce</a></li>
                <li role="presentation"><a href="stageaffectation.php">Affectation des étudiants</a></li>
                <li role="presentation"><a href="planning.php">Planification des soutenances</a></li>
                <li role="presentation"><a href="notes.php">Affectation des notes </a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br/>

    <h4 class="text-center">Gestion du planning des soutenances</h4>
    <br/>
    <br/>
    <div class="row">
        <div class="col-sm-2">
            <fieldset><legend>Liste des stages </legend>
                <form method="post" action="planning.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="sel_stage">Sélectionner un stage : </label>
                        <select class="form-control input-sm" id="sel_stage" name="sel_stage">
                            <?php
                            $select_stage = mysqli_query($link,"SELECT e.id_etu, e.nom, e.prenom, s.id_annonce, s.id_stage, en.nom
                                                                  FROM stage AS s INNER JOIN etudiant AS e, annonce AS an, entreprise AS en
                                                                  WHERE s.id_stage = e.id_stage AND an.id_ent = en.id_ent
                                                                  AND s.id_annonce = an.id_annonce AND s.id_sou IS NULL ;") or die(mysqli_error($link));
                            while($resultat_select_stage = mysqli_fetch_array($select_stage))
                            {
                                ?>
                                <option value="<?php echo $resultat_select_stage[4]; ?>"> <?php echo $resultat_select_stage[1]." ".$resultat_select_stage[2]." - Entreprise : ".$resultat_select_stage[5]; ?> </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </fieldset>
        </div>

        <div class=" col-sm-9" id="tableau_soutenance">
            <fieldset><legend>Récapitulatif</legend>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="text-primary">
                        <td class="hidden">Id Soutenance</td>
                        <td>&Eacute;lève</td>
                        <td>Entreprise</td>
                        <td style="width: 28%">Sujet</td>
                        <td>Tuteur entreprise</td>
                        <td>Tuteur enseignant</td>
                        <td>Co-Enseignant</td>
                        <td>Date et Heure</td>
                        <td>Salle</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $selection_soutenances = mysqli_query($link,"SELECT sou.id_sou, et.nom, et.prenom, en.nom, an.sujet_annonce, an.nom_responsable, ens.nom_enseignant, ens.prenom_enseignant, ens1.nom_enseignant, ens1.prenom_enseignant, sou.date_sou, sou.heure_sou , sa.libelle_salle
                                                                FROM soutenance AS sou
                                                                INNER JOIN entreprise AS en, annonce AS an, enseignant AS ens, salle AS sa, stage AS st, etudiant AS et, enseignant AS ens1
                                                                WHERE st.id_sou = sou.id_sou
                                                                AND sou.id_enseignant = ens.id_enseignant
                                                                AND sou.id_enseignant_1 = ens1.id_enseignant
                                                                AND sou.id_salle = sa.id_salle
                                                                AND et.id_stage = st.id_stage
                                                                AND st.id_annonce = an.id_annonce
                                                                AND an.id_ent = en.id_ent
                                                                ;") or die(mysqli_error($link));
                    while($resultat_soutenance = mysqli_fetch_array($selection_soutenances))
                    {
                        ?>
                        <tr>
                            <td class="hidden" id="id_soutenance<?php echo $resultat_soutenance[0];?>"> <?php echo $resultat_soutenance[0];?> </td>
                            <td> <?php echo $resultat_soutenance[1]." ".$resultat_soutenance[2];?> </td>
                            <td> <?php echo $resultat_soutenance[3];?> </td>
                            <td> <?php echo $resultat_soutenance[4];?> </td>
                            <td> <?php echo $resultat_soutenance[5];?> </td>
                            <td> <?php echo $resultat_soutenance[6]." ".$resultat_soutenance[7];?> </td>
                            <td> <?php echo $resultat_soutenance[8]." ".$resultat_soutenance[9];?> </td>
                            <td> <?php echo "Date : ".$resultat_soutenance[10]."<br/> Heure : ".$resultat_soutenance[11];?> </td>
                            <td> <?php echo $resultat_soutenance[12];?> </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <fieldset><legend>Date et Heure </legend>
                <form method="post" action="planning.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="date_sou">Date :</label>
                        <input class="form-control input-sm" type="date" id="date_sou" name="date_sou" step="1" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="heure_sou">Heure :</label>
                        <input class="form-control input-sm" type="time" id="heure_sou" name="heure_sou" onblur="choixDate();" />
                    </div>
                </form>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <div id="wait_gif" class="text-center">
                <img src="img/ajax-loader.gif" alt="GIF" id="gif_loader">
            </div>
        </div>
    </div>
    <div class="row" id="div_salle">
        <div class="col-sm-2">
            <fieldset><legend> Salle </legend>
                <form method="post" action="planning.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="select_salle">Salle :</label>
                        <div class="input-group">
                            <select class="form-control input-sm" id="select_salle" name="select_salle" onblur="choixEnseignant();">

                            </select>
                            <a href="#" class="input-group-addon glyphicon-plus btn btn-info btn-sm" data-toggle="modal" data-target="#modal_salle"> Ajouter</a>
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <div id="wait_gif1" class="text-center">
                <img src="img/ajax-loader.gif" alt="GIF" id="gif_loader">
            </div>
        </div>
    </div>
    <div class="row" id="div_tuteurs">
        <div class="col-sm-2">
            <fieldset><legend> Tuteurs </legend>
                <form method="post" action="planning.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="select_enseignant">Tuteur Enseignant :</label>
                        <select class="form-control input-sm" id="select_enseignant" name="select_enseignant" >

                        </select>
                        <label class="control-label" for="select_coenseignant">Co-Enseignant :</label>
                        <select class="form-control input-sm" id="select_coenseignant" name="select_coenseignant">

                        </select>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>

    <div class="row" id="div_btn_planning">
        <div class="col-sm-2 text-center">
            <hr/>
            <button class="btn btn-success" type="button" onclick="affecterSoutenance();">Ajouter</button>
            <a href="espace_ens.php" class="btn btn-danger">Annuler</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <hr/>
            <a href="espace_ens.php" class="btn btn-warning">Retour</a>
        </div>
    </div>

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_salle" tabindex="-1" role="dialog" aria-labelledby="title_modal4">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal4"> Ajouter une salle</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="planning.php" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label" for="lib_salle">Numéro de la salle :</label>
                            <input class="form-control input-sm" type="text" id="lib_salle" name="lib_salle" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" onclick="ajoutSalle();" data-dismiss="modal">Valider</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Annuler</button>
                </div>
            </div>
        </div>
    </div>

<?php
}
include('footer.php');
?>