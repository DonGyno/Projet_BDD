<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 13/11/2015
 * Time: 14:28
 */
include("config.php");

if(isset($_GET['action']) && $_GET['action'] == "deconnexion")
{
    unset($_SESSION);
    session_destroy();
    echo "Vous êtes à présent déconnecté !";
}

if(!isset($_GET["action"])) {
?>

    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="js/jquery-1.11.3.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/Project_BDD.js" type="text/javascript"></script>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING : Respond.js doesn't work if you view the page via file:// -->

        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/Project_BDD.css" type="text/css">

        <title>Project - BDD</title>
    </head>

<body class="container" id="MyBody">

    <div class="row" id="conteneur_IMG">
        <div class="col-sm-4">
            <a href="http://www.iut-bm.univ-fcomte.fr/"> <img src="img/logo_iut.jpg" class="img-responsive" id="Logo_IUT"/>
            </a>
        </div>
        <div class="col-sm-4">
            <h3 class="text-info text-center">Stages & Informations</h3>
        </div>
        <div class="col-sm-4">
            <a href="http://www.univ-fcomte.fr/"> <img src="img/logo_ufc.png" class="img-responsive" id="Logo_UFC"/> </a>
        </div>
    </div>
    <hr/>

    <div class="row" id="Breadcrumb">
        <div class="col-sm-10">
            <li>
                <?php
                $index = "index";
                $chemin_page = $_SERVER['PHP_SELF'];
                $chemin_decoupe = explode("/", $chemin_page);

                echo('<a href="/">Accueil</a> > ');
                for ($i = 1; $i < count($chemin_decoupe); $i++) {
                    echo('<a href="/');
                    for ($j = 1; $j <= $i; $j++) {
                        echo($chemin_decoupe[$j]);
                        if ($j != count($chemin_decoupe) - 1) {
                            echo("/");
                        }
                    }

                    if ($i == count($chemin_decoupe) - 1) {
                        $chemin_prec = explode(".", $chemin_decoupe[$i]);
                        if ($chemin_prec[0] == $index) $chemin_prec[0] = "";
                        $chemin_prec[0] = $chemin_prec[0] . "</a>";
                    } else $chemin_prec[0] = $chemin_decoupe[$i] . '</a> > ';
                    echo('">');
                    echo(str_replace("_", " ", ucfirst($chemin_prec[0])));
                }
                ?>
            </li>
        </div>
    </div>

    <div class="modal fade" id="modal_deco" tabindex="-1" role="dialog" aria-labelledby="title_modal2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal2"> Déconnexion </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_deco">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

<!-- Fin Header -->
<?php
}
?>