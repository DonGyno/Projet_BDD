<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 14/11/2015
 * Time: 17:23
 */

// Modifier les requêtes sql , les basculer en prepare / execute ! récupère dernier ID avec insert_id

// Accueil mettre quelques informations (derniers stages, soutenance , annonce ...)


include("header.php");

//Traitement Connexion et redirection dans l'espace Etudiant ou Enseignant
if(isset($_GET['action']) && $_GET['action'] == "connexion_etu")
{
    // Récupération des données par AJAX, On les protèges des différentes injections SQL, JAVASCRIPT ETC ...
    $login_etu = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtolower($_POST["login_etu"]))));
    $pwd_etu = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["pwd_etu"])));

    if(!empty($login_etu) && !empty($pwd_etu))
    {
        // On vérifie la présence du login et mot de passe dans la base de donnée.
        $verif_loginEtu = mysqli_query($link,"SELECT * FROM etudiant WHERE login = '".$login_etu."' AND pwd = '".$pwd_etu."';") or die(mysqli_error($link));
        $resultat = mysqli_fetch_array($verif_loginEtu);
        if(($resultat["login"] == $login_etu) && ($resultat["pwd"] == $pwd_etu ))
        {
            echo "1-Connexion réussie ! Vous allez être redirigé automatiquement ou appuyer sur Ok.";
            $_SESSION["id_etudiant"] = $resultat["id_etu"];
            $_SESSION["nom_etudiant"] = $resultat["nom"];
            $_SESSION["prenom_etudiant"] = $resultat["prenom"];
            $_SESSION["login_etudiant"] = $resultat["login"];
            $_SESSION["pwd_etudiant"] = $resultat["pwd"];
            $_SESSION["type"] = "etudiant";
        }
        else
        {
            echo "2-Une erreur est survenue ! Le login ou le mot de passe est incorrect !";
        }
    }
    else
    {
        echo "3-Les champs ne sont pas tous renseignés !";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "connexion_ens")
{
    // Récupération des données par AJAX, On les protèges des différentes injections SQL, JAVASCRIPT ETC ...
    $login_ens = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtolower($_POST["login_ens"]))));
    $pwd_ens = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["pwd_ens"])));

    if(!empty($login_ens) && !empty($pwd_ens))
    {
        // On vérifie la présence du login et mot de passe dans la base de donnée.
        $verif_loginEns = mysqli_query($link,"SELECT * FROM enseignant WHERE login = '".$login_ens."' AND pwd = '".$pwd_ens."';") or die(mysqli_error($link));
        $resultat = mysqli_fetch_array($verif_loginEns);
        if(($resultat["login"] == $login_ens) && ($resultat["pwd"] == $pwd_ens))
        {
            echo "1-Connexion réussie ! Vous allez être redirigé automatiquement ou appuyer sur Ok.";
            $_SESSION["id_enseignant"] = $resultat["id_enseignant"];
            $_SESSION["nom_enseignant"] = $resultat["nom_enseignant"];
            $_SESSION["prenom_enseignant"] = $resultat["prenom_enseignant"];
            $_SESSION["login_enseignant"] = $resultat["login"];
            $_SESSION["pwd_enseignant"] = $resultat["pwd"];
            $_SESSION["type"] = "enseignant";
        }
        else
        {
            echo "2-Une erreur est survenue ! Le login ou le mot de passe est incorrect !";
        }
    }
    else
    {
        echo "3-Les champs ne sont pas tous renseignés !";
    }
}


if(!isset($_GET["action"]))
{
?>

    <div class="row">
        <h4 class="text-center" id="h4_Index">Bienvenue</h4>
    </div>

    <hr/>
    <?php
    if(empty($_SESSION))
    {
    ?>
        <div class="row">
            <div class="col-sm-offset-1 col-sm-5">
                <div class="panel panel-primary" id="panel_etu">
                    <div class="panel-heading">
                        Vous êtes &Eacute;tudiant ? C'est par là..
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="index.php">
                            <div class="form-group">
                                <label class="control-label" for="login_etu">Login :</label>
                                <input class="form-control input-sm" type="text" name="login_etu" id="login_etu"
                                       placeholder="Votre login.."/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="pwd_etu">Mot de passe :</label>
                                <input class="form-control input-sm" type="password" name="pwd_etu" id="pwd_etu"
                                       placeholder="Votre mot de passe.."/>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-sm" id="valider_etu" name="valider_etu" onclick="connexion_etu();">
                                    Valider
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <p class="text-center text-success">
                            Pas encore inscrit ? C'est <a href="inscription.php">ici</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-primary" id="panel_etu">
                    <div class="panel-heading">
                        Vous êtes Enseignant ? C'est par ici..
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="index.php">
                            <div class="form-group">
                                <label class="control-label" for="login_ens">Login :</label>
                                <input class="form-control input-sm" type="text" name="login_ens" id="login_ens"
                                       placeholder="Votre login.."/>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="pwd_ens">Mot de passe :</label>
                                <input class="form-control input-sm" type="password" name="pwd_ens" id="pwd_ens"
                                       placeholder="Votre mot de passe.."/>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-sm" id="valider_ens" name="valider_ens" onclick="connexion_ens();">
                                    Valider
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="panel-footer">
                        <p class="text-center text-success">
                            Pas encore inscrit ? C'est <a href="inscription.php">ici</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    <?php
    }
    else
    {
        ?>
        <div class="row">
            <div class="col-sm-4 center-block" style="float: none;">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Déjà connecté
                    </div>
                    <div class="panel-body">
                        <p class="text-info">
                            Vous êtes déjà connecté en tant que <span class="text-danger"> <?php if($_SESSION["type"] == "enseignant"){
                                echo $_SESSION["login_enseignant"]; echo "</span><hr/> Cliquez <a href='espace_ens.php'>ici</a> pour revenir à l'espace enseignant.";}
                                if($_SESSION["type"] == "etudiant"){ echo $_SESSION["login_etudiant"];
                                    echo "</span><hr/> Cliquez <a href='espace_etu.php'>ici</a> pour revenir à l'espace étudiant.";} ?>
                            <br/>
                            <hr/>
                        </p>
                        <p class="text-info">
                            Ce n'est pas vous ? Alors cliquez <a href="#" onclick="deconnexion_session();">ici</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

<?php
}
include("footer.php");
?>