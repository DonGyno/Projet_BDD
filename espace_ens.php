<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 23/11/2015
 * Time: 17:15
 */
include('header.php');

// Vérification si un enseignant est bien connecté en vérifiant ses variables de SESSION Sinon on l'alerte
// et on le redirige à l'index.
if(empty($_SESSION["login_enseignant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("espace_ens.php");
        });
    </script>
    <?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}


if(!isset($_GET["action"]))
{
?>

    <div class="row">
        <div class="col-sm-12">
            <h4 class="text-center">Bievenue sur votre espace <?php if(!empty($_SESSION["login_enseignant"])){echo $_SESSION["prenom_enseignant"]; }?> !</h4>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="poster_annonce.php">Poster une annonce</a></li>
                <li role="presentation"><a href="stageaffectation.php">Affectation des étudiants</a></li>
                <li role="presentation"><a href="planning.php">Planification des soutenances</a></li>
                <li role="presentation"><a href="notes.php">Affectation des notes </a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

<?php
}
include('footer.php');
?>