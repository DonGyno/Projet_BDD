<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 07/12/2015
 * Time: 17:10
 */
include('header.php');

if(empty($_SESSION["login_enseignant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("stageaffectation.php");
        });
    </script>
    <?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(isset($_GET['action']) && $_GET['action'] == "affectation")
{
    $id_annonce = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_annonce"])));
    $id_etudiant = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_etudiant"])));
    if(!empty($id_annonce) && !empty($id_etudiant))
    {
        // Insertion du stage en premier lieu
        $query = "INSERT INTO stage VALUES (NULL, '".$id_annonce."', NULL) ;";
        $stmt = $link->prepare($query);
        $stmt->execute();
        //récupération de l'id du stage ajouté
        $last_ID = $stmt->insert_id;


        // Modification de l'étudiant, on lui attribue le stage dans l'id_stage
        $query1="UPDATE etudiant SET id_stage = '".$last_ID."' WHERE id_etu = '".$id_etudiant."' ;";
        $stmt = $link->prepare($query1);
        $stmt->execute();

        //Modification de l'annonce on la met non disponible
        $query2="UPDATE annonce SET disponible = 0 WHERE id_annonce = '".$id_annonce."' ;";
        $stmt = $link->prepare($query2);
        $stmt->execute();

        echo "1|L'affectation est un succès ! ";

    }
    else
    {
        echo "2|Veuillez bien sélectionner une annonce et un étudiant !";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "selection_stage")
{
    $id_annonce = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_annonce"])));
    $selection_sujet_annonce = mysqli_query($link,"SELECT an.sujet_annonce, mc.libelle_motscles FROM annonce AS an
                                                   INNER JOIN annoncemotscles AS anmc, motscles AS mc
                                                   WHERE an.id_annonce = '".$id_annonce."' AND an.id_annonce = anmc.id_annonce
                                                   AND mc.id_motscles = anmc.id_motscles
                                                   ;") or die(mysqli_error($link));
    while($resultat_sujet_annonce = mysqli_fetch_array($selection_sujet_annonce))
    {
        echo $resultat_sujet_annonce[0]." - ".$resultat_sujet_annonce[1]." ;";
    }

}

if(!isset($_GET["action"]))
{
    ?>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="poster_annonce.php">Poster une annonce</a></li>
                <li role="presentation"><a href="stageaffectation.php">Affectation des étudiants</a></li>
                <li role="presentation"><a href="planning.php">Planification des soutenances</a></li>
                <li role="presentation"><a href="notes.php">Affectation des notes </a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br />

    <h3 class="text-center">Affectation de stage</h3>
    <br/>
    <br/>
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1">
            <fieldset id="liste_stage_dispo"><legend>Liste des stages disponibles</legend>
                <form method="post" action="stageaffectation.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="sel_stage">Sélectionner un stage : </label>
                        <select class="form-control input-sm" id="sel_stage" name="sel_stage" onblur="show_sujet();">
                            <?php
                                $select_stage_dispo = mysqli_query($link,"SELECT a.id_annonce, a.sujet_annonce, a.nom_responsable, e.nom
                                                      FROM annonce AS a INNER JOIN entreprise AS e
                                                      WHERE a.disponible = 1 AND a.id_ent = e.id_ent ;") or die(mysqli_error($link));
                                while($result_stage_dispo = mysqli_fetch_array($select_stage_dispo))
                                {
                                    ?>
                                    <option value="<?php echo $result_stage_dispo[0]; ?>"> <?php echo $result_stage_dispo[3]." - ".$result_stage_dispo[2]; ?> </option>
                                    <?php
                                }
                            ?>
                        </select>
                        <br/>
                        <div id="wait_gif" class="text-center">
                            <img src="img/ajax-loader.gif" alt="GIF" id="gif_loader">
                        </div>
                        <div class="panel panel-primary" id="panel_hide">
                            <div class="panel-heading">
                                Sujet du stage
                            </div>
                            <div class="panel-body">
                                <p class="text-primary" id="panel_sujet">

                                </p>
                            </div>
                            <div class="panel-footer">

                                <span> Mots clés : <span class="glyphicon glyphicon-tags"> </span></span>
                                <p class="text-primary" id="panel_motsclefs">

                                </p>
                            </div>
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="col-sm-5">
            <fieldset id="liste_stage_dispo"><legend>Liste des étudiants non-affectés : </legend>
                <form method="post" action="stageaffectation.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="sel_etu">Sélectionner un étudiant : </label>
                        <select class="form-control input-sm" id="sel_etu" name="sel_etu">
                            <?php
                            $select_etudiant_nonaffecte = mysqli_query($link,"SELECT etu.id_etu, etu.nom, etu.prenom, gr.libelle_groupe FROM etudiant AS etu
                                                          INNER JOIN groupe AS gr
                                                          WHERE etu.id_stage IS NULL AND etu.id_groupe = gr.id_groupe ;") or die(mysqli_error($link));
                            while($resultat_etudiant_nonaffecte = mysqli_fetch_array($select_etudiant_nonaffecte))
                            {
                                ?>
                                <option value="<?php echo $resultat_etudiant_nonaffecte[0]; ?>"> <?php echo $resultat_etudiant_nonaffecte[1]." ".$resultat_etudiant_nonaffecte[2]." | Groupe : ".$resultat_etudiant_nonaffecte[3]; ?> </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="row text-center col-sm-12">
            <button class="btn btn-success" type="button" onclick="affecter_etudiant_stage();">Valider</button>
            <a href="espace_ens.php" class="btn btn-danger">Annuler</a>
        </div>
    </div>


    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

    <?php
}

include('footer.php');
?>