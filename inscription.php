<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 23/11/2015
 * Time: 17:16
 */
include('header.php');

if(isset($_GET['action']) && $_GET['action'] == "inscription")
{
    //Récupération et protection des données reçus.
    $regime = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["regime"])));
    $nom = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["nom"])));
    $prenom = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["prenom"])));
    $login = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtolower($_POST["login"]))));
    $pwd1 = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["pwd1"])));
    $pwd2 = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["pwd2"])));
    $groupe = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["groupe"])));

    //Vérifie si les données ne sont pas vide
    if(!empty($regime) && !empty($nom) && !empty($prenom) && !empty($login) && !empty($pwd1) && !empty($pwd2))
    {
        // On vérifie les mots de passe si ils sont corrects.
        if($pwd1 == $pwd2)
        {
            // On vérifie également le régime choisi
            if($regime == "etudiant" && !empty($groupe))
            {
                //On vérifie la présence d'un login similaire vu qu'il doit etre unique
                $select_login = mysqli_query($link,"SELECT login FROM etudiant WHERE login = '".$login."';") or die(mysqli_error($link));
                $resultat = mysqli_fetch_array($select_login);
                if($resultat["login"] != $login)
                {
                    //On peut faire les requêtes pour l'inscription d'un étudiant
                    mysqli_query($link,"INSERT INTO etudiant VALUES(NULL,'".$nom."','".$prenom."','".$login."','".$pwd1."',".$groupe.",NULL);") or die(mysqli_error($link));
                    echo "1-Inscription réussie ! Vous pouvez dès maintenant vous connecter à votre espace !";
                }
                else
                {
                    echo "2-Erreur ! Le login est déjà utilisé ! Veuillez réessayer.";
                }
            }
            else
            {
                $select_login = mysqli_query($link,"SELECT login FROM enseignant WHERE login = '".$login."';") or die(mysqli_error($link));
                $resultat = mysqli_fetch_array($select_login);
                if($resultat["login"] != $login)
                {
                    //On peut faire les requêtes pour l'inscription d'un enseignant
                    mysqli_query($link,"INSERT INTO enseignant VALUES(NULL,'".$nom."','".$prenom."','".$login."','".$pwd1."');") or die(mysqli_error($link));
                    echo "1-Inscription réussie ! Vous pouvez dès maintenant vous connecter à votre espace !";
                }
                else
                {
                    echo "2-Erreur ! Le login est déjà utilisé ! Veuillez réessayer.";
                }
            }
        }
        else
        {
            echo "3-Erreur ! Les mots de passes ne sont pas identiques !";
        }
    }
    else
    {
        echo "4-Erreur ! Les champs ne sont pas tous renseignés !";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "ajout_groupe")
{
    $libgroupe = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtoupper($_POST["lib_groupe"]))));
    if(!empty($libgroupe))
    {
        $sel_groupe = mysqli_query($link,"SELECT libelle_groupe FROM groupe WHERE libelle_groupe = '".$libgroupe."';") or die(mysqli_query($link));
        $results = mysqli_fetch_array($sel_groupe);
        if($results["libelle_groupe"] != $libgroupe)
        {
            mysqli_query($link,"INSERT INTO groupe VALUES(NULL,'".$libgroupe."')") or die(mysqli_error($link));
            echo "1|Ajout du groupe réussie !";
        }
        else
        {
            echo "2|Erreur ! Ce groupe existe déjà !";
        }
    }
    else
    {
        echo "3|Erreur ! Le champ est vide !";
    }
}

if(!isset($_GET["action"]))
{
?>

    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
            <fieldset> <legend class="text-center">Inscription</legend>
                <form method="post" action="inscription.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="sel_reg">Votre régime :</label>
                        <select name="sel_reg" id="sel_reg" class="form-control input-sm" onclick="groupe_test();">
                            <option value="enseignant" selected>Enseignant</option>
                            <option value="etudiant">&Eacute;tudiant</option>
                        </select>
                    </div>
                    <div class="form-group" id="groupe_div">
                        <label class="control-label" for="sel_grp">Votre groupe :</label>
                        <div class="input-group">
                            <select name="sel_grp" id="sel_grp" class="form-control input-sm">
                                <?php
                                $select_groupe = mysqli_query($link,"SELECT * FROM groupe ORDER BY libelle_groupe;") or die(mysqli_error($link));
                                while($result = mysqli_fetch_array($select_groupe))
                                {
                                    ?>
                                    <option value="<?php echo $result["id_groupe"]; ?>"> <?php echo $result["libelle_groupe"]; ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <a href="#" class="input-group-addon input-sm glyphicon-plus btn btn-info btn-sm" data-toggle="modal" data-target="#modal_groupe"> Ajouter</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="prenom">Votre prénom :</label>
                        <input class="form-control input-sm" type="text" name="prenom" id="prenom" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="nom">Votre nom :</label>
                        <input class="form-control input-sm" type="text" name="nom" id="nom" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="login">Votre login :</label>
                        <input class="form-control input-sm" type="text" name="login" id="login" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pwd1">Votre mot de passe :</label>
                        <input class="form-control input-sm" type="password" name="pwd1" id="pwd1" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pwd2">Retaper votre mot de passe :</label>
                        <input class="form-control input-sm" type="password" name="pwd2" id="pwd2" />
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="button" onclick="inscription();">Valider</button>
                        <a href="index.php" class="btn btn-danger">Annuler</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>

    <div class="modal fade" id="modal_groupe" tabindex="-1" role="dialog" aria-labelledby="title_modal2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal2"> Ajout d'un groupe</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="inscription.php" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label" for="lib_groupe">Libellé du groupe :</label>
                            <input class="form-control input-sm" type="text" name="lib_groupe" id="lib_groupe" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" onclick="ajout_groupe();" data-dismiss="modal">Valider</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Annuler</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>


<?php
}
include('footer.php');
?>