<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 08/10/2015
 * Time: 10:34
 */
// Modifier selon votre cas 
$HOST = "localhost";
$USER = "root";
$MDP = "";
$BDD = "projetbdd";
//Connexion à la BDD
$link = mysqli_connect($HOST,$USER,$MDP,$BDD);
$link->set_charset("utf8");

mysqli_query($link,"SET NAMES UTF8") or die(mysqli_error($link));

date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR.utf8','fra');

session_start();

?>