<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 12/12/2015
 * Time: 20:27
 */
include('header.php');


if(isset($_GET['action']) && $_GET['action'] == "ajoutNote")
{
    $id_soutenance = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_soutenance"])));
    $note_entreprise = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["note_entreprise"])));
    $note_soutenance = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["note_soutenance"])));
    $note_rapport = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["note_rapport"])));

    if(!empty($note_entreprise) && !empty($note_soutenance) && !empty($note_rapport))
    {
        //Insertion des notes dans la table note
        $query = "INSERT INTO notes VALUES (NULL,'".$note_entreprise."', '".$note_soutenance."', '".$note_rapport."') ;";
        $stmt = $link->prepare($query);
        $stmt->execute();
        $last_ID_Note = $stmt->insert_id;

        mysqli_query($link,"UPDATE soutenance AS sou SET sou.id_note = '".$last_ID_Note."'  WHERE sou.id_sou = '".$id_soutenance."' ;") or die(mysqli_error($link));
        echo "1| Ajout des notes réussie !";
    }
    else
    {
        echo "2|Une erreur est survenue ! Des champs ne sont pas remplies";
    }

}


if(!isset($_GET["action"]))
{
?>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="poster_annonce.php">Poster une annonce</a></li>
                <li role="presentation"><a href="stageaffectation.php">Affectation des étudiants</a></li>
                <li role="presentation"><a href="planning.php">Planification des soutenances</a></li>
                <li role="presentation"><a href="notes.php">Affectation des notes </a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br />

    <h4 class="text-center">Gestion des notes</h4>
    <br/>
    <br/>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-4">
            <fieldset><legend>Soutenance</legend>
                <form method="post" action="notes.php" class="form-horizontal">
                    <div class="form-group" id="div_select_soutenance">
                        <label class="control-label" for="select_soutenance">Sélection de la soutenance :</label>
                        <select id="select_soutenance" name="select_soutenance" class="form-control input-sm" onblur="$('#field_note').show(800);">
                            <?php
                            $selection_soutenances = mysqli_query($link,"SELECT sou.id_sou ,etu.nom, etu.prenom FROM soutenance AS sou
                                                                        INNER JOIN stage AS sta, etudiant AS etu
                                                                        WHERE sou.id_sou = sta.id_sou AND etu.id_stage = sta.id_stage
                                                                        AND sou.id_note IS NULL;") or die (mysqli_error($link));
                            while($resultat_soutenances = mysqli_fetch_array($selection_soutenances))
                            {
                                ?>
                                <option value="<?php echo $resultat_soutenances[0]; ?>"> <?php echo "Soutenance de ".$resultat_soutenances[1]." ".$resultat_soutenances[2]; ?> </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </fieldset>
        </div>
        <div class="col-sm-4" id="field_note">
            <fieldset><legend>Ajout des notes</legend>
                <form method="post" action="notes.php" class="form-horizontal" id="div_ajout_note">
                    <div class="form-group">
                        <label class="control-label" for="note_entreprise">Note de l'entreprise :</label>
                        <div class="input-group input-group-sm"">
                            <input class="form-control input-sm" type="number" id="note_entreprise" name="note_entreprise" min="0" max="6" aria-describedby="addon1" />
                            <span class="input-group-addon" id="addon1"> /6</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="note_soutenance">Note de la soutenance :</label>
                        <div class="input-group input-group-sm"">
                            <input class="form-control input-sm" type="number" id="note_soutenance" name="note_soutenance" min="0" max="10" aria-describedby="addon2" />
                            <span class="input-group-addon" id="addon2"> /10</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="note_rapport">Note des rapports :</label>
                        <div class="input-group input-group-sm">
                            <input class="form-control input-sm" type="number" id="note_rapport" name="note_rapport" min="0" max="4" aria-describedby="addon3" />
                            <span class="input-group-addon" id="addon3"> /4</span>
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <hr/>
            <button class="btn btn-success" type="button" onclick="ajoutNote();">Ajouter</button>
            <a href="espace_ens.php" class="btn btn-danger">Annuler</a>
        </div>
    </div>


    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>


<?php
}
include('footer.php')
?>