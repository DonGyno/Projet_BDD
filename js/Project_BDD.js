/**
 * Created by Guillaume on 13/11/2015.
 */

$(document).ready(function(){
    $("#groupe_div").hide();
    $("#panel_hide").hide();
    $("#wait_gif").hide();
    $("#wait_gif1").hide();
    $("#field_note").hide();
    $("#div_salle").hide();
    $("#div_tuteurs").hide();
    $("#div_btn_planning").hide();
});

function groupe_test()
{
    if($("#sel_reg").val() == "etudiant")
    {
        $("#groupe_div").show(800);
    }
    else
    {
        $("#groupe_div").hide(800);
    }
}

function getXHR()
{
    var xhr = null;

    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch(e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }

    return xhr;
}

function connexion_etu()
{
    var xhr = getXHR();
    var login = document.getElementById('login_etu').value;
    var pwd = document.getElementById('pwd_etu').value;
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("-");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute('onclick',"redirection_etu()");
                setTimeout(redirection_etu,3000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "index.php?action=connexion_etu", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("login_etu="+login+"&pwd_etu="+pwd);
}

function connexion_ens()
{
    var xhr = getXHR();
    var login = document.getElementById('login_ens').value;
    var pwd = document.getElementById('pwd_ens').value;
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("-");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute('onclick',"redirection_ens()");
                setTimeout(redirection_ens,3000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "index.php?action=connexion_ens", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("login_ens="+login+"&pwd_ens="+pwd);
}

function verif_login(page)
{
    var xhr = getXHR();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText;
            span.innerHTML = info;
            $("#modal_infos").modal("show");
            btn_rac.setAttribute('onclick',"redirection_index()");
            setTimeout(redirection_index,3000);
        }
    }
    xhr.open("POST", page+"?action=verif", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send(null);
}

function deconnexion_session()
{
    var xhr = getXHR();
    var span = document.getElementById("span_deco");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText;
            span.innerHTML = info;
            $("#modal_deco").modal("show");
            btn_rac.setAttribute('onclick',"redirection_index()");
            setTimeout(redirection_index,2000);
        }
    }
    xhr.open("POST", "header.php?action=deconnexion", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send(null);
}

function inscription()
{
    var xhr = getXHR();
    var regime = document.getElementById('sel_reg').value;
    var nom = document.getElementById('nom').value;
    var prenom = document.getElementById('prenom').value;
    var login = document.getElementById('login').value;
    var pwd1 = document.getElementById('pwd1').value;
    var pwd2 = document.getElementById('pwd2').value;
    var groupe = document.getElementById('sel_grp').value;
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("-");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute('onclick',"redirection_index()");
                setTimeout(redirection_index,2000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "inscription.php?action=inscription", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("regime="+regime+"&nom="+nom+"&prenom="+prenom+"&login="+login+"&pwd1="+pwd1+"&pwd2="+pwd2+"&groupe="+groupe);
}

function ajout_groupe()
{
    var xhr = getXHR();
    var libgroupe = $("#lib_groupe").val();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('inscription.php')");
                setTimeout(function(){
                    refresh_page("inscription.php");
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "inscription.php?action=ajout_groupe", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("lib_groupe="+libgroupe);
}

function ajout_entreprise()
{
    var xhr = getXHR();
    var nom_ent = $("#nom_ent").val();
    var secteur = $("#secteur_ent").val();
    var tel = $("#tel_ent").val();
    var adr_ent = $("#adr_ent").val();
    var ville_ent = $("#ville_ent").val();
    var cp_ent = $("#cp_ent").val();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("-");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('poster_annonce.php')");
                setTimeout(function(){
                    refresh_page("poster_annonce.php");
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "poster_annonce.php?action=ajout_entreprise", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("nom_ent="+nom_ent+"&secteur="+secteur+"&adr_ent="+adr_ent+"&ville_ent="+ville_ent+"&cp_ent="+cp_ent+"&tel_ent="+tel);
}

function ajout_annonce()
{
    var xhr = getXHR();
    var id_ent = $("#sel_ent").val();
    var sujet_annonce = $("#sujet_annonce").val();
    var mots_clefs = $("#mots_clefs").val();
    var responsable = $("#responsable").val();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","redirection_ens()");
                setTimeout(function(){
                    redirection_ens();
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "poster_annonce.php?action=ajout_annonce", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("id_ent="+id_ent+"&sujet_annonce="+sujet_annonce+"&mots_clefs="+mots_clefs+"&responsable="+responsable);
}

function show_sujet()
{
    var xhr = getXHR();
    var p_panel = document.getElementById("panel_sujet");
    var panel_mots = document.getElementById("panel_motsclefs");
    var id_annonce = $("#sel_stage").val();
    $("#panel_hide").hide(800,function()
    {
        p_panel.innerHTML="";
        panel_mots.innerHTML="";
        $("#wait_gif").fadeIn("slow");
    });

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var infos = xhr.responseText.split(";");
            var sujet = xhr.responseText.split("-");
            setTimeout(function(){
                $("#wait_gif").fadeOut("slow");
                for(var i=0; i<infos.length-1; i++)
                {
                    var info = infos[i].split("-");
                    panel_mots.innerHTML += info[1]+" / ";
                }
                p_panel.innerHTML = sujet[0];
                $("#panel_hide").show(800);
            },1300);

        }
    }
    xhr.open("POST", "stageaffectation.php?action=selection_stage", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("id_annonce="+id_annonce);
}

function affecter_etudiant_stage()
{
    var xhr = getXHR();
    var id_annonce = $("#sel_stage").val();
    var id_etudiant = $("#sel_etu").val();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('stageaffectation.php')");
                setTimeout(function(){
                    refresh_page('stageaffectation.php');
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "stageaffectation.php?action=affectation", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("id_annonce="+id_annonce+"&id_etudiant="+id_etudiant);
}

function choixDate()
{
    var xhr = getXHR();
    var date_sou = document.getElementById("date_sou").value;
    var heure_sou = document.getElementById("heure_sou").value;
    var sel_salle = document.getElementById("select_salle");
    $("#div_salle").hide(800,function()
    {
        $("#wait_gif1").fadeIn("slow");
        sel_salle.innerHTML = "";
    });

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var salles = xhr.responseText.split(";");
            setTimeout(function(){
                $("#wait_gif1").fadeOut("slow");
                for(var i=0; i<salles.length-1; i++)
                {
                    var salle = salles[i].split("-");
                    var option = document.createElement("option");
                    option.setAttribute("value",salle[0]);
                    option.innerHTML = salle[1];
                    sel_salle.appendChild(option);
                }
                $("#div_salle").show(800);
            },1000);
        }
    }
    xhr.open("POST", "planning.php?action=choixDate", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("date_sou="+date_sou+"&heure_sou="+heure_sou);
}

function choixEnseignant()
{
    var xhr = getXHR();
    var date_sou = document.getElementById("date_sou").value;
    var heure_sou = document.getElementById("heure_sou").value;

    var select_enseignant = document.getElementById("select_enseignant");
    $("#div_tuteurs").hide(800,function()
    {
        $("#wait_gif1").fadeIn("slow");
        select_enseignant.innerHTML="";
    });

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var enseignants = xhr.responseText.split(";");
            setTimeout(function(){
                $("#wait_gif1").fadeOut("slow");
                for(var i=0; i<enseignants.length-1; i++)
                {
                    var enseignant = enseignants[i].split("-");
                    var option = document.createElement("option");
                    option.setAttribute("value",enseignant[0]);
                    option.innerHTML = enseignant[1] + " " + enseignant[2];

                    select_enseignant.appendChild(option);
                }

                $("#div_tuteurs").show(800);
            },1000);
            select_enseignant.setAttribute("onblur","choixCoEnseignant()");
        }
    }
    xhr.open("POST", "planning.php?action=choixEnseignant", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("date_sou="+date_sou+"&heure_sou="+heure_sou);
}

function choixCoEnseignant()
{
    var xhr = getXHR();
    var date_sou = document.getElementById("date_sou").value;
    var heure_sou = document.getElementById("heure_sou").value;

    var select_coenseignant = document.getElementById("select_coenseignant");
    select_coenseignant.innerHTML = "";

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var enseignants = xhr.responseText.split(";");
            for(var i=0; i<enseignants.length-1; i++)
            {
                var enseignant = enseignants[i].split("-");
                var option1 = document.createElement("option");
                option1.setAttribute("value",enseignant[0]);
                option1.innerHTML = enseignant[1]+ " " + enseignant[2];

                select_coenseignant.appendChild(option1);
            }
            $("#div_btn_planning").show(800);
        }
    }
    xhr.open("POST", "planning.php?action=choixCoEnseignant", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("date_sou="+date_sou+"&heure_sou="+heure_sou);
}

function affecterSoutenance()
{
    var xhr = getXHR();
    var id_stage = document.getElementById("sel_stage").value;
    var date_sou = document.getElementById("date_sou").value;
    var heure_sou = document.getElementById("heure_sou").value;
    var id_salle = document.getElementById("select_salle").value;
    var id_enseignant = document.getElementById("select_enseignant").value;
    var id_coenseignant = document.getElementById("select_coenseignant").value;

    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('planning.php')");
                setTimeout(function(){
                    refresh_page('planning.php');
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "planning.php?action=affecterSoutenance", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("id_stage="+id_stage+"&date_sou="+date_sou+"&heure_sou="+heure_sou+"&id_salle="+id_salle+"&id_enseignant="+id_enseignant+"&id_coenseignant="+id_coenseignant);
}

function ajoutSalle()
{
    var xhr = getXHR();
    var lib_salle = $("#lib_salle").val();
    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('planning.php')");
                setTimeout(function(){
                    refresh_page('planning.php');
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "planning.php?action=ajoutSalle", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("lib_salle="+lib_salle);
}

function ajoutNote()
{
    var xhr = getXHR();
    var id_soutenance = document.getElementById("select_soutenance").value;
    var note_entreprise = document.getElementById("note_entreprise").value;
    var note_soutenance = document.getElementById("note_soutenance").value;
    var note_rapport = document.getElementById("note_rapport").value;

    var span = document.getElementById("span_infos");
    span.innerHTML = "";
    var btn_rac = document.getElementById("raccourci_btn");
    xhr.onreadystatechange=function()
    {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
        {
            var info = xhr.responseText.split("|");
            if(info[0] == 1)
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
                btn_rac.setAttribute("onclick","refresh_page('notes.php')");
                setTimeout(function(){
                    refresh_page('notes.php');
                },4000);
            }
            else
            {
                span.innerHTML = info[1];
                $("#modal_infos").modal("show");
            }
        }
    }
    xhr.open("POST", "notes.php?action=ajoutNote", true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xhr.send("id_soutenance="+id_soutenance+"&note_entreprise="+note_entreprise+"&note_soutenance="+note_soutenance+"&note_rapport="+note_rapport);
}


function redirection_etu()
{
    document.location.href = "espace_etu.php";
}

function redirection_ens()
{
    document.location.href = "espace_ens.php";
}

function redirection_index()
{
    document.location.href = "index.php";
}

function refresh_page(page)
{
    document.location.href = page;
}