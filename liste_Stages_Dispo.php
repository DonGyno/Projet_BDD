<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 03/12/2015
 * Time: 07:18
 */

include('header.php');

// Vérification si un étudiant est bien connecté en vérifiant ses variables de SESSION Sinon on l'alerte
// et on le redirige à l'index.
if(empty($_SESSION["login_etudiant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("liste_stages_dispo.php");
        });
    </script>
<?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(!isset($_GET["action"])) {
    ?>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="liste_Stages_Dispo.php">Liste des stages disponibles</a></li>
                <li role="presentation"><a href="planning_etu.php">Planning des soutenances</a></li>
                <li role="presentation"><a href="listes_notes.php">Votre note</a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br />

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

    <?php


        $total = mysqli_query($link,"SELECT entreprise.nom, adresse.libelle_adresse, ville.libelle_ville, secteur_activite.libelle_secteur, annonce.sujet_annonce FROM entreprise
                                     INNER JOIN adresse,ville,secteur_activite,annonce,adresseentreprise
                                     WHERE entreprise.id_ent = adresseentreprise.id_ent
                                     AND adresse.id_adresse = adresseentreprise.id_adresse
                                     AND adresse.id_ville = ville.id_ville
                                     AND secteur_activite.id_secteur = entreprise.id_secteur
                                     AND annonce.id_ent = entreprise.id_ent
                                     AND annonce.disponible = '1';") or die(mysqli_error($link));
?>
    <br/>
    <table class="table table-bordered table-hover">
        <thead>
        <tr class="text-primary">
            <th>Entreprise</th>
            <th>Adresse</th>
            <th>Ville</th>
            <th>Secteur</th>
            <th>Stage propose</th>
        </tr>
        </thead>
<?php

        while($donnees =mysqli_fetch_array($total)){

?>



             <tbody>
                <tr>
                    <td><?php echo $donnees["nom"]; ?></td>
                    <td><?php echo $donnees["libelle_adresse"]; ?></td>
                    <td><?php echo $donnees["libelle_ville"]; ?></td>
                    <td><?php echo $donnees["libelle_secteur"]; ?></td>
                    <td><?php echo $donnees["sujet_annonce"]; ?></td>
                </tr>
             </tbody>
<?php
            }
?>
    </table>
<?php
    include('footer.php');
}
?>