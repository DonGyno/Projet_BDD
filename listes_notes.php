<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 14/12/2015
 * Time: 11:05
 */

include('header.php');

// Vérification si un étudiant est bien connecté en vérifiant ses variables de SESSION Sinon on l'alerte
// et on le redirige à l'index.
if(empty($_SESSION["login_etudiant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("listes_notes.php");
        });
    </script>
<?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(!isset($_GET["action"])) {
        ?>


    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="liste_Stages_Dispo.php">Liste des stages disponibles</a></li>
                <li role="presentation"><a href="planning_etu.php">Planning des soutenances</a></li>
                <li role="presentation"><a href="listes_notes.php">Votre note</a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>
        
    <br />

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

    <?php

        $utilisateur = $_SESSION["login_etudiant"];

        $note = mysqli_query($link,"SELECT * FROM notes
                                    INNER JOIN soutenance,stage,etudiant
                                    WHERE notes.id_note = soutenance.id_note
                                    AND soutenance.id_sou = stage.id_sou
                                    AND stage.id_stage = etudiant.id_stage
                                    AND etudiant.login = '$utilisateur';")or die(mysqli_error($link));
        ?>
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Note du rapport</th>
                <th>note de l'entreprise</th>
                <th>Note de la soutenance</th>
                <th>Moyenne</th>
            </tr>
            </thead>

    <?php
        while($donnees =mysqli_fetch_array($note)){

            $moyenne = $donnees["note_rapport"] + $donnees["note_entreprise"] + $donnees["note_soutenance"];
            $moyenne = number_format($moyenne,2);


            ?>

                <tbody >
                    <tr>
                        <td><?php echo $donnees["note_entreprise"]; ?></td>
                        <td><?php  echo $donnees["note_soutenance"]; ?></td>
                        <td><?php echo $donnees["note_rapport"]; ?></td>
                        <td><?php echo $moyenne; ?></td>
                    </tr>
                </tbody>

<?php

        }
    ?>
    </table>
<?php
}
include('footer.php');
?>