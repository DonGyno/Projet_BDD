-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 18 Décembre 2015 à 11:09
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `projetbdd`
--
CREATE DATABASE IF NOT EXISTS `projetbdd` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `projetbdd`;

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE IF NOT EXISTS `adresse` (
  `id_adresse` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_adresse` varchar(100) DEFAULT NULL,
  `id_ville` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_adresse`),
  KEY `FK_adresse_id_ville` (`id_ville`),
  FULLTEXT KEY `libelle_adresse` (`libelle_adresse`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `adresse`
--

INSERT INTO `adresse` (`id_adresse`, `libelle_adresse`, `id_ville`) VALUES
(1, '13 rue des forges', 1),
(2, '15 avenue marechal juin', 2),
(3, '84 rue des lilas', 3),
(4, '65 avenue des champs elysees', 3),
(5, '65 rue des distingues', 4),
(6, '36 rue saint-desire', 4),
(7, '74 rue des espoirs', 5),
(8, '4 rue des coquelicots', 2),
(9, '6 rue de pompidou', 6),
(10, '5 rue charles de gaulle', 3);

-- --------------------------------------------------------

--
-- Structure de la table `adresseentreprise`
--

CREATE TABLE IF NOT EXISTS `adresseentreprise` (
  `id_ent` int(11) NOT NULL,
  `id_adresse` int(11) NOT NULL,
  PRIMARY KEY (`id_ent`,`id_adresse`),
  KEY `FK_adresseEntreprise_id_adresse` (`id_adresse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `adresseentreprise`
--

INSERT INTO `adresseentreprise` (`id_ent`, `id_adresse`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10);

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE IF NOT EXISTS `annonce` (
  `id_annonce` int(11) NOT NULL AUTO_INCREMENT,
  `sujet_annonce` longtext,
  `nom_responsable` varchar(50) DEFAULT NULL,
  `disponible` tinyint(1) DEFAULT NULL,
  `id_ent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_annonce`),
  KEY `FK_annonce_id_ent` (`id_ent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `annonce`
--

INSERT INTO `annonce` (`id_annonce`, `sujet_annonce`, `nom_responsable`, `disponible`, `id_ent`) VALUES
(1, 'Vous devez développer un logiciel permettant de capter les différentes données thermiques pour ensuite les insérer dans une base de donnée MySQL.', 'Faramir Raoul', 0, 3),
(2, 'Développement d''une application de gestion du personnel en python. ', 'Georges Lucas', 0, 8),
(3, 'Amélioration du site web actuel afin de répondre à différent besoin des utilisateurs. ', 'Patrick simon', 0, 10),
(4, 'Migration d''un site web.', 'ZENEO Romain', 1, 9);

-- --------------------------------------------------------

--
-- Structure de la table `annoncemotscles`
--

CREATE TABLE IF NOT EXISTS `annoncemotscles` (
  `id_annonce` int(11) NOT NULL,
  `id_motscles` int(11) NOT NULL,
  PRIMARY KEY (`id_annonce`,`id_motscles`),
  KEY `FK_annonceMotscles_id_motscles` (`id_motscles`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `annoncemotscles`
--

INSERT INTO `annoncemotscles` (`id_annonce`, `id_motscles`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(3, 6);

-- --------------------------------------------------------

--
-- Structure de la table `codepostal`
--

CREATE TABLE IF NOT EXISTS `codepostal` (
  `id_codepostal` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_codepostal` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_codepostal`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `codepostal`
--

INSERT INTO `codepostal` (`id_codepostal`, `libelle_codepostal`) VALUES
(1, '39300'),
(2, '90000'),
(3, '75000'),
(4, '39000'),
(5, '90230'),
(6, '25000');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE IF NOT EXISTS `enseignant` (
  `id_enseignant` int(11) NOT NULL AUTO_INCREMENT,
  `nom_enseignant` varchar(50) DEFAULT NULL,
  `prenom_enseignant` varchar(25) DEFAULT NULL,
  `login` varchar(25) DEFAULT NULL,
  `pwd` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_enseignant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `enseignant`
--

INSERT INTO `enseignant` (`id_enseignant`, `nom_enseignant`, `prenom_enseignant`, `login`, `pwd`) VALUES
(1, 'Dupond', 'Jean', 'jdupond', 'dupond'),
(2, 'Weber', 'Bernard', 'bweber', 'weber'),
(3, 'Duhamel', 'Josh', 'jduha', 'josh');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `id_ent` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `id_secteur` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ent`),
  KEY `FK_entreprise_id_secteur` (`id_secteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`id_ent`, `nom`, `id_secteur`) VALUES
(1, 'HYPERBOLE', 6),
(2, 'ALSTOM', 2),
(3, 'GENERAL ELECTRICS', 7),
(4, 'WEBAVENTURE', 6),
(5, 'ILLICOWEB', 6),
(6, 'PRODESSA', 8),
(7, 'MANAGRICOL', 1),
(8, 'IBM', 2),
(9, 'ZENEO', 2),
(10, 'SNCF', 2);

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE IF NOT EXISTS `etudiant` (
  `id_etu` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `login` varchar(25) DEFAULT NULL,
  `pwd` varchar(25) DEFAULT NULL,
  `id_groupe` int(11) DEFAULT NULL,
  `id_stage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_etu`),
  KEY `FK_etudiant_id_groupe` (`id_groupe`),
  KEY `FK_etudiant_id_stage` (`id_stage`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `etudiant`
--

INSERT INTO `etudiant` (`id_etu`, `nom`, `prenom`, `login`, `pwd`, `id_groupe`, `id_stage`) VALUES
(1, 'Jourdain', 'Guillaume', 'gjourdai', 'guigui', 1, 2),
(2, 'Bak', 'Benjamin', 'bbak', 'bak', 1, NULL),
(3, 'Norui', 'Patrick', 'pnorui', 'norui', 2, 1),
(4, 'Perron', 'Fanny', 'fperron', 'perron', 3, 3),
(5, 'Karaf', 'Charles', 'ckaraf', 'karaf', 4, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE IF NOT EXISTS `groupe` (
  `id_groupe` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_groupe` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_groupe`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `groupe`
--

INSERT INTO `groupe` (`id_groupe`, `libelle_groupe`) VALUES
(1, 'LICENCE-TEPROW'),
(2, 'S4-A1'),
(3, 'S4-A2'),
(4, 'S3-A1'),
(5, 'S3-A2');

-- --------------------------------------------------------

--
-- Structure de la table `motscles`
--

CREATE TABLE IF NOT EXISTS `motscles` (
  `id_motscles` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_motscles` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_motscles`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `motscles`
--

INSERT INTO `motscles` (`id_motscles`, `libelle_motscles`) VALUES
(1, 'MYSQL'),
(2, 'BDD'),
(3, 'LOGICIEL'),
(4, 'PYTHON'),
(5, 'APLICATION'),
(6, 'WEB');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id_note` int(11) NOT NULL AUTO_INCREMENT,
  `note_entreprise` float DEFAULT NULL,
  `note_soutenance` float DEFAULT NULL,
  `note_rapport` float DEFAULT NULL,
  PRIMARY KEY (`id_note`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id_note`, `note_entreprise`, `note_soutenance`, `note_rapport`) VALUES
(1, 6, 4, 3),
(2, 6, 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE IF NOT EXISTS `salle` (
  `id_salle` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_salle` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_salle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `salle`
--

INSERT INTO `salle` (`id_salle`, `libelle_salle`) VALUES
(1, '103'),
(2, '105'),
(3, '207'),
(4, '108'),
(5, '205');

-- --------------------------------------------------------

--
-- Structure de la table `secteur_activite`
--

CREATE TABLE IF NOT EXISTS `secteur_activite` (
  `id_secteur` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_secteur` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_secteur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `secteur_activite`
--

INSERT INTO `secteur_activite` (`id_secteur`, `libelle_secteur`) VALUES
(1, 'Agriculture et Agroalimentaire'),
(2, 'Industrie'),
(3, 'Énergie'),
(4, 'Commerce et Artisanat'),
(5, 'Tourisme'),
(6, 'Télécoms et Internet'),
(7, 'Recherche'),
(8, 'Finance et Assurance');

-- --------------------------------------------------------

--
-- Structure de la table `soutenance`
--

CREATE TABLE IF NOT EXISTS `soutenance` (
  `id_sou` int(11) NOT NULL AUTO_INCREMENT,
  `date_sou` date DEFAULT NULL,
  `heure_sou` time DEFAULT NULL,
  `id_enseignant` int(11) DEFAULT NULL,
  `id_enseignant_1` int(11) DEFAULT NULL,
  `id_salle` int(11) DEFAULT NULL,
  `id_note` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sou`),
  KEY `FK_soutenance_id_enseignant` (`id_enseignant`),
  KEY `FK_soutenance_id_enseignant_1` (`id_enseignant_1`),
  KEY `FK_soutenance_id_salle` (`id_salle`),
  KEY `FK_soutenance_id_note` (`id_note`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `soutenance`
--

INSERT INTO `soutenance` (`id_sou`, `date_sou`, `heure_sou`, `id_enseignant`, `id_enseignant_1`, `id_salle`, `id_note`) VALUES
(1, '2016-01-12', '10:00:00', 1, 2, 1, 1),
(2, '2016-01-12', '11:00:00', 2, 1, 3, 2),
(3, '2016-01-14', '10:00:00', 1, 3, 5, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `stage`
--

CREATE TABLE IF NOT EXISTS `stage` (
  `id_stage` int(11) NOT NULL AUTO_INCREMENT,
  `id_annonce` int(11) DEFAULT NULL,
  `id_sou` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_stage`),
  KEY `FK_stage_id_annonce` (`id_annonce`),
  KEY `FK_stage_id_sou` (`id_sou`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `stage`
--

INSERT INTO `stage` (`id_stage`, `id_annonce`, `id_sou`) VALUES
(1, 1, 2),
(2, 2, 1),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Structure de la table `telephone`
--

CREATE TABLE IF NOT EXISTS `telephone` (
  `id_tel` int(11) NOT NULL AUTO_INCREMENT,
  `num_tel` varchar(25) DEFAULT NULL,
  `id_ent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tel`),
  KEY `FK_telephone_id_ent` (`id_ent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `telephone`
--

INSERT INTO `telephone` (`id_tel`, `num_tel`, `id_ent`) VALUES
(1, '0398785862', 2),
(2, '0385463215', 3),
(3, '0498653245', 4),
(4, '0365874521', 5),
(5, '0384569874', 6),
(6, '0303030303', 7),
(7, '0387252658', 8),
(8, '0638595858', 9),
(9, '0125859685', 10);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id_ville` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_ville` varchar(75) DEFAULT NULL,
  `id_codepostal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ville`),
  KEY `FK_ville_id_codepostal` (`id_codepostal`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id_ville`, `libelle_ville`, `id_codepostal`) VALUES
(1, 'Champagnole', 1),
(2, 'Belfort', 2),
(3, 'Paris', 3),
(4, 'Lons-le-saunier', 4),
(5, 'Bavilliers', 5),
(6, 'Besancon', 6);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `FK_adresse_id_ville` FOREIGN KEY (`id_ville`) REFERENCES `ville` (`id_ville`);

--
-- Contraintes pour la table `adresseentreprise`
--
ALTER TABLE `adresseentreprise`
  ADD CONSTRAINT `FK_adresseEntreprise_id_adresse` FOREIGN KEY (`id_adresse`) REFERENCES `adresse` (`id_adresse`),
  ADD CONSTRAINT `FK_adresseEntreprise_id_ent` FOREIGN KEY (`id_ent`) REFERENCES `entreprise` (`id_ent`);

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `FK_annonce_id_ent` FOREIGN KEY (`id_ent`) REFERENCES `entreprise` (`id_ent`);

--
-- Contraintes pour la table `annoncemotscles`
--
ALTER TABLE `annoncemotscles`
  ADD CONSTRAINT `FK_annonceMotscles_id_annonce` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id_annonce`),
  ADD CONSTRAINT `FK_annonceMotscles_id_motscles` FOREIGN KEY (`id_motscles`) REFERENCES `motscles` (`id_motscles`);

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `FK_entreprise_id_secteur` FOREIGN KEY (`id_secteur`) REFERENCES `secteur_activite` (`id_secteur`);

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `FK_etudiant_id_groupe` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id_groupe`),
  ADD CONSTRAINT `FK_etudiant_id_stage` FOREIGN KEY (`id_stage`) REFERENCES `stage` (`id_stage`);

--
-- Contraintes pour la table `soutenance`
--
ALTER TABLE `soutenance`
  ADD CONSTRAINT `FK_soutenance_id_enseignant` FOREIGN KEY (`id_enseignant`) REFERENCES `enseignant` (`id_enseignant`),
  ADD CONSTRAINT `FK_soutenance_id_enseignant_1` FOREIGN KEY (`id_enseignant_1`) REFERENCES `enseignant` (`id_enseignant`),
  ADD CONSTRAINT `FK_soutenance_id_note` FOREIGN KEY (`id_note`) REFERENCES `notes` (`id_note`),
  ADD CONSTRAINT `FK_soutenance_id_salle` FOREIGN KEY (`id_salle`) REFERENCES `salle` (`id_salle`);

--
-- Contraintes pour la table `stage`
--
ALTER TABLE `stage`
  ADD CONSTRAINT `FK_stage_id_annonce` FOREIGN KEY (`id_annonce`) REFERENCES `annonce` (`id_annonce`),
  ADD CONSTRAINT `FK_stage_id_sou` FOREIGN KEY (`id_sou`) REFERENCES `soutenance` (`id_sou`);

--
-- Contraintes pour la table `telephone`
--
ALTER TABLE `telephone`
  ADD CONSTRAINT `FK_telephone_id_ent` FOREIGN KEY (`id_ent`) REFERENCES `entreprise` (`id_ent`);

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `FK_ville_id_codepostal` FOREIGN KEY (`id_codepostal`) REFERENCES `codepostal` (`id_codepostal`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
