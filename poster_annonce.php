<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 26/11/2015
 * Time: 16:21
 */
include('header.php');
// Vérification si un enseignant est bien connecté en vérifiant ses variables de SESSION Sinon on l'alerte
// et on le redirige à l'index.
if(empty($_SESSION["login_enseignant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("poster_annonce.php");
        });
    </script>
    <?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(isset($_GET['action']) && $_GET['action'] == "ajout_entreprise")
{
    //Récupération de données par l'AJAX
    $nom_ent = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtoupper($_POST["nom_ent"]))));
    $secteur = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["secteur"])));
    $adr_ent = mysqli_real_escape_string($link,strtolower($_POST["adr_ent"])); // problème accent
    $ville_ent = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(htmlentities(ucfirst(strtolower($_POST["ville_ent"]))))));
    $cp_ent = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["cp_ent"])));
    $tel_ent = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["tel_ent"])));


    if(!empty($nom_ent) && !empty($secteur) && !empty($adr_ent) && !empty($ville_ent) && !empty($cp_ent) && !empty($tel_ent))
    {
        // Vérifie si l'entreprise est déjà présente avec l'adresse , la ville et le code postal indiqué dans le formulaire grâce aux clés étrangères
        $select_entr = mysqli_query($link,"SELECT entreprise.id_ent, entreprise.nom, adresse.libelle_adresse, ville.libelle_ville, codepostal.libelle_codepostal FROM entreprise
                                                  INNER JOIN adresseentreprise, adresse, ville, codepostal
                                                  WHERE ((entreprise.nom = '".$nom_ent."') OR (adresse.libelle_adresse = '".$adr_ent."' AND ville.libelle_ville = '".$ville_ent."' AND codepostal.libelle_codepostal = '".$cp_ent."'))
                                                  AND entreprise.id_ent = adresseentreprise.id_ent
                                                  AND adresseentreprise.id_adresse = adresse.id_adresse
                                                  AND adresse.id_ville = ville.id_ville
                                                  AND ville.id_codepostal = codepostal.id_codepostal ;") or die(mysqli_error($link));
        $result_entr = mysqli_fetch_array($select_entr);

        if(($result_entr[1] == $nom_ent) || ($result_entr[2] == $adr_ent && $result_entr[3] == $ville_ent && $result_entr[4] == $cp_ent))
        {
            echo "2-Un problème est survenue ! L'entreprise ".$nom_ent." est déjà crée ou bien l'adresse ".$adr_ent." ".$cp_ent." ".$ville_ent." fournie est déjà utilisé.";
        }
        else
        {
            // Vérif de l'entreprise
            $select_entreprise = mysqli_query($link,"SELECT nom, id_ent FROM entreprise WHERE nom = '".$nom_ent."' ;") or die(mysqli_error($link));
            $resultat_entreprise = mysqli_fetch_array($select_entreprise);
            if($resultat_entreprise[0] != $nom_ent)
            {
                $query = "INSERT INTO entreprise VALUES (NULL,'".$nom_ent."','".$secteur."');";
                $stmt = $link->prepare($query);
                $stmt->execute();
                $last_ID_Entreprise = $stmt->insert_id;
            }

            // Pas besoin de vérifié l'adresse on peut avoir le même nom de rue dans une ville différente etc ..
            $query1 = "INSERT INTO adresse VALUES (NULL,'".$adr_ent."',NULL) ;";
            $stmt = $link->prepare($query1);
            $stmt->execute();
            $last_ID_Adresse = $stmt->insert_id;

            // Vérif de la ville
            $select_ville = mysqli_query($link,"SELECT id_ville, libelle_ville FROM ville WHERE libelle_ville = '".$ville_ent."' ;") or die(mysqli_error($link));
            $resultat_ville = mysqli_fetch_array($select_ville);
            if($resultat_ville[1] != $ville_ent)
            {
                $query2 = "INSERT INTO ville VALUES (NULL,'".$ville_ent."',NULL);";
                $stmt = $link->prepare($query2);
                $stmt->execute();
                $last_ID_Ville = $stmt->insert_id;
            }
            else
            {
                $last_ID_Ville = $resultat_ville[0];
            }

            // Vérif du code postal
            $select_codepostal = mysqli_query($link,"SELECT id_codepostal, libelle_codepostal FROM codepostal WHERE libelle_codepostal = '".$cp_ent."' ;") or die(mysqli_error($link));
            $resultat_codepostal = mysqli_fetch_array($select_codepostal);
            if($resultat_codepostal[1] != $cp_ent)
            {
                $query3 = "INSERT INTO codepostal VALUES (NULL,'".$cp_ent."');";
                $stmt = $link->prepare($query3);
                $stmt->execute();
                $last_ID_Cp = $stmt->insert_id;
            }
            else
            {
                $last_ID_Cp = $resultat_codepostal[0];
            }

            //Update Ville - Code postal (clé étrangère)
            $query4 = "UPDATE ville SET id_codepostal = '".$last_ID_Cp."' WHERE id_ville = '".$last_ID_Ville."' ;";
            $stmt = $link->prepare($query4);
            $stmt->execute();

            //Update Adresse - Ville
            $query5 = "UPDATE adresse SET id_ville = '".$last_ID_Ville."' WHERE id_adresse = '".$last_ID_Adresse."' ;";
            $stmt = $link->prepare($query5);
            $stmt->execute();

            //Insert AdresseEntreprise
            $query6 = "INSERT INTO adresseentreprise VALUES ('".$last_ID_Entreprise."','".$last_ID_Adresse."');";
            $stmt = $link->prepare($query6);
            $stmt->execute();

            //Insert téléphone
            $query7 = "INSERT INTO telephone VALUES (NULL,'".$tel_ent."','".$last_ID_Entreprise."');";
            $stmt = $link->prepare($query7);
            $stmt->execute();

            echo "1-Ajout de l'entreprise réussie !";
        }
    }
    else
    {
        echo "2-Les champs ne sont pas tous renseignés !";
    }
}

if(isset($_GET['action']) && $_GET['action'] == "ajout_annonce")
{
    //Récupération des données par AJAX
    $id_ent = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["id_ent"])));
    $sujet_annonce= mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["sujet_annonce"])));
    $mots_clefs = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes(strtoupper($_POST["mots_clefs"]))));
    $responsable = mysqli_real_escape_string($link,htmlspecialchars(stripcslashes($_POST["responsable"])));

    if(!empty($id_ent) && !empty($sujet_annonce) && !empty($mots_clefs) && !empty($responsable))
    {
        // Vérifie si une annonce est déjà présente avce l'id de l'entreprise choisi : Si il en trouve alors erreur sinon on l'ajoute.
        $select_annonce = mysqli_query($link,"SELECT id_ent FROM annonce WHERE id_ent = '".$id_ent."'") or die(mysqli_error($link));
        $result_annonce = mysqli_fetch_array($select_annonce);
        if($result_annonce[0] != $id_ent)
        {
            // On doit également insérer les mots clès dans la base de donnée !
            $tab_motsclefs = explode(" ",$mots_clefs); // Permet de séparer les mots clès délimité par un espace
            if(sizeof($tab_motsclefs) <= 4)
            {
                // On peut insérer l'annonce dans la base de donnée.
                $query8 = "INSERT INTO annonce VALUES (NULL,'".$sujet_annonce."','".$responsable."',1,'".$id_ent."') ;";
                $stmt = $link->prepare($query8);
                $stmt->execute();
                $last_ID_Annonce = $stmt->insert_id;

                foreach ($tab_motsclefs as $key)
                {
                    $select_motsClef = mysqli_query($link,"SELECT libelle_motscles, id_motscles FROM motscles WHERE libelle_motscles = '".$key."'") or die(mysqli_error($link));
                    $result_motsClef = mysqli_fetch_array($select_motsClef);
                    if($result_motsClef[0] != $key)
                    {
                        $query9 = "INSERT INTO motscles VALUES (NULL, '".$key."') ;";
                        $stmt = $link->prepare($query9);
                        $stmt->execute();
                        $last_ID_MotClef = $stmt->insert_id;

                        $query10 = "INSERT INTO annoncemotscles VALUES ('".$last_ID_Annonce."','".$last_ID_MotClef."');";
                        $stmt = $link->prepare($query10);
                        $stmt->execute();
                    }
                }

                echo "1|Annonce ajouté avec succès ! Redirection à votre espace.";
            }
            else
            {
                echo "2|Vous avez mis trop de mots clés ! 4 au maximum.";
            }

        }
        else
        {
            echo "2|Une annonce concernant l'entreprise sélectionné est déjà présente dans la base de donnée !";
        }
    }
    else
    {
        echo "2|Les champs ne sont pas tous renseignés !";
    }
}

if(!isset($_GET["action"]))
{
?>

    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="poster_annonce.php">Poster une annonce</a></li>
                <li role="presentation"><a href="stageaffectation.php">Affectation des étudiants</a></li>
                <li role="presentation"><a href="planning.php">Planification des soutenances</a></li>
                <li role="presentation"><a href="notes.php">Affectation des notes </a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br />

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <fieldset> <legend class="text-center">Poster une annonce de stage</legend>
                <form method="post" action="poster_annonce.php" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="sel_ent">Sélectionner l'entreprise : </label>
                        <div class="input-group">
                            <select class="form-control input-sm" name="sel_ent" id="sel_ent">
                                <?php
                                $select_ent = mysqli_query($link,"SELECT DISTINCT ent.id_ent, ent.nom, adr.libelle_adresse, cp.libelle_codepostal, vi.libelle_ville  FROM entreprise AS ent
                                                                  INNER JOIN adresse AS adr, ville AS vi, codepostal AS cp, adresseentreprise AS adrEnt
                                                                  WHERE adr.id_adresse = adrEnt.id_adresse AND ent.id_ent = adrEnt.id_ent
                                                                  AND adr.id_ville = vi.id_ville AND vi.id_codepostal = cp.id_codepostal
                                                                  ;") or die(mysqli_error($link));
                                while($result = mysqli_fetch_array($select_ent))
                                {
                                    ?>
                                        <option value="<?php echo $result[0]; ?>"> <?php echo $result[1]; ?> <?php echo " | Adresse : ".$result[2]." ".$result[3]." ".$result[4]; ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <a href="#" class="input-group-addon input-sm glyphicon-plus btn btn-info btn-sm" data-toggle="modal" data-target="#modal_entreprise"> Ajouter</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="responsable">Responsable : </label>
                        <input class="form-control input-sm" type="text" id="responsable" name="responsable" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sujet_annonce">Sujet :</label>
                        <textarea class="form-control input-sm" id="sujet_annonce" name="sujet_annonce" rows="6" ></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="mots_clefs">Mots-clés : <em>(Maximum 4)</em></label>
                        <input class="form-control input-sm" type="text" id="mots_clefs" name="mots_clefs" />
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="button" onclick="ajout_annonce();">Valider</button>
                        <a href="espace_ens.php" class="btn btn-danger">Annuler</a>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>


    <div class="modal fade" id="modal_entreprise" tabindex="-1" role="dialog" aria-labelledby="title_modal3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal3"> Ajouter une entreprise </h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="poster_annonce.php" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label" for="nom_ent">Nom entreprise :</label>
                            <input class="form-control input-sm" type="text" id="nom_ent" name="nom_ent" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="secteur_ent">Secteur d'activité :</label>
                            <select class="form-control input-sm" id="secteur_ent" name="secteur_ent">
                                <?php
                                $select_secteur = mysqli_query($link,"SELECT * FROM secteur_activite ORDER BY libelle_secteur;") or die(mysqli_error($link));
                                while($resultat = mysqli_fetch_array($select_secteur))
                                {
                                    ?>
                                    <option value="<?php echo $resultat["id_secteur"]; ?>"> <?php echo $resultat["libelle_secteur"]; ?> </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="tel_ent">Téléphone : </label>
                            <input class="form-control input-sm" type="tel" id="tel_ent" name="tel_ent"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="adr_ent">Adresse : </label>
                            <input class="form-control input-sm" type="text" id="adr_ent" name="adr_ent" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="ville_ent">Ville : </label>
                            <input class="form-control input-sm" type="text" id="ville_ent" name="ville_ent" />
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="cp_ent">Code postal : </label>
                            <input class="form-control input-sm" type="text" id="cp_ent" name="cp_ent" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="button" onclick="ajout_entreprise();" data-dismiss="modal">Valider</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Annuler</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>


<?php
}
?>