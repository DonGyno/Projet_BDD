<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 03/12/2015
 * Time: 07:18
 */

include('header.php');

// Vérification si un étudiant est bien connecté en vérifiant ses variables de SESSION Sinon on l'alerte
// et on le redirige à l'index.
if(empty($_SESSION["login_etudiant"]))
{
    ?>
    <script>$(document).ready(function(){
            verif_login("liste_stages_dispo.php");
        });
    </script>
<?php
}

if(isset($_GET['action']) && $_GET['action'] == "verif")
{
    echo "Vous devez être connecté pour visualiser cette page ! Redirection en cours..";
}

if(!isset($_GET["action"])) {
    ?>


    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs nav-justified" id="menu_etu">
                <li role="presentation"><a href="liste_Stages_Dispo.php">Liste des stages disponibles</a></li>
                <li role="presentation"><a href="planning_etu.php">Planning des soutenances</a></li>
                <li role="presentation"><a href="listes_notes.php">Votre note</a></li>
                <li role="presentation"><a href="#" onclick="deconnexion_session();">Déconnexion</a></li>
            </ul>
        </div>
    </div>

    <br />

    <div class="modal fade" id="modal_infos" tabindex="-1" role="dialog" aria-labelledby="title_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="title_modal"> Informations </h4>
                </div>
                <div class="modal-body">
                    <span class="alert-info" id="span_infos">   </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="raccourci_btn"> Ok</button>
                </div>
            </div>
        </div>
    </div>

<div class="table table-bordered table-hover">
            <fieldset> <legend>Planning des soutenances</legend>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="text-primary">
                        <td class="hidden">Id Soutenance</td>
                        <td>&Eacute;lève</td>
                        <td>Entreprise</td>
                        <td style="width: 28%">Sujet</td>
                        <td>Tuteur entreprise</td>
                        <td>Tuteur enseignant</td>
                        <td>Co-Enseignant</td>
                        <td>Date et Heure</td>
                        <td>Salle</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
    $selection_soutenances = mysqli_query($link,"SELECT sou.id_sou, et.nom, et.prenom, en.nom, an.sujet_annonce, an.nom_responsable, ens.nom_enseignant, ens.prenom_enseignant, ens1.nom_enseignant, ens1.prenom_enseignant, sou.date_sou, sou.heure_sou , sa.libelle_salle
                                                                FROM soutenance AS sou
                                                                INNER JOIN entreprise AS en, annonce AS an, enseignant AS ens, salle AS sa, stage AS st, etudiant AS et, enseignant AS ens1
                                                                WHERE st.id_sou = sou.id_sou
                                                                AND sou.id_enseignant = ens.id_enseignant
                                                                AND sou.id_enseignant_1 = ens1.id_enseignant
                                                                AND sou.id_salle = sa.id_salle
                                                                AND et.id_stage = st.id_stage
                                                                AND st.id_annonce = an.id_annonce
                                                                AND an.id_ent = en.id_ent
                                                                ;") or die(mysqli_error($link));
    while($resultat_soutenance = mysqli_fetch_array($selection_soutenances))
    {
        ?>
        <tr>
            <td class="hidden" id="id_soutenance<?php echo $resultat_soutenance[0];?>"> <?php echo $resultat_soutenance[0];?> </td>
            <td> <?php echo $resultat_soutenance[1]." ".$resultat_soutenance[2];?> </td>
            <td> <?php echo $resultat_soutenance[3];?> </td>
            <td> <?php echo $resultat_soutenance[4];?> </td>
            <td> <?php echo $resultat_soutenance[5];?> </td>
            <td> <?php echo $resultat_soutenance[6]." ".$resultat_soutenance[7];?> </td>
            <td> <?php echo $resultat_soutenance[8]." ".$resultat_soutenance[9];?> </td>
            <td> <?php echo "Date : ".$resultat_soutenance[10]."<br/> Heure : ".$resultat_soutenance[11];?> </td>
            <td> <?php echo $resultat_soutenance[12];?> </td>
        </tr>
    <?php
    }
    ?>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
    <?php








    include('footer.php');
}
?>